function personnage(prmNom,PrmNiveau) {
    this.Nom = prmNom;
    this.Niveau = PrmNiveau;
}

personnage.prototype.saluer = function () {
    let saluer;
    saluer = this.Nom+" vous salue!!"
    return saluer;
}

function guerrier(prmNom,prmNiveau,prmArmes) {
    personnage.call(this,prmNom,prmNiveau)
    this.Armes = prmArmes;
}

guerrier.prototype = Object.create(personnage.prototype);

function Magicien(prmNom,prmNiveau,prmSorts) {
    personnage.call(this,prmNom,prmNiveau)
    this.Sorts = prmSorts;
}

Magicien.prototype = Object.create(personnage.prototype);

guerrier.prototype.combattre = function() {
    let dialogue="";
    dialogue = this.Nom + ' est un guerrier qui se bat avec '+this.Armes;
    return dialogue;
}

Magicien.prototype.posseder = function() {
    let dialogue="";
    dialogue = this.Nom + ' est un magicien qui posséde le pouvoir de '+this.Sorts;
    return dialogue;
}

let objperso1 = new guerrier('Arthur',3,'épée');
let objperso2 = new Magicien('Merlin',2,'prédire les batailles');
console.log(objperso1.saluer());
console.log(objperso1.combattre());
console.log(objperso2.saluer());
console.log(objperso2.posseder());