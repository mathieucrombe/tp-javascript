/*
Auteur : Mathieu Crombe
Date : 26/04/2021
*/
//Version 1
/*let objPatient = {
    nom : 'Dupond',
    prenom : 'jean',
    age : 30,
    sexe : 'masculin',
    pronom : 'il',
    taille : 180, //cm
    poids : 85, //kg
    decrire : function() {
        let description="";
        description = "le patient "+ this.prenom +" "+this.nom+" de sexe "+this.sexe+" est agé de "+this.age+" ans. Il mesure "+this.taille+" cm et pèse "+this.poids+" kg"; 
        return description;
    },
    CalculerImc : function() {
      let calculimc = 0;
      calculimc = this.poids / ((this.taille / 100) * (this.taille / 100));
      return calculimc;  
    },
    interpreter_IMC: function(prmIMC) {
        let interpretation = "";
    if (prmIMC < 16.5) {
        interpretation =this.pronom+" est en situation "+ "de denutrition";
    } else if ((prmIMC >= 16.5) && (prmIMC < 18.5)) {
        interpretation =this.pronom+" est en situation "+ "de maigreur";
    } else if ((prmIMC >= 18.5) && (prmIMC < 25)) {
        interpretation =this.pronom+" est en situation "+ "de corpulence normal";
    } else if ((prmIMC >= 25) && (prmIMC < 30)) {
        interpretation =this.pronom+" est en situation "+ "de surpoids";
    } else if ((prmIMC >= 30) && (prmIMC < 35)) {
        interpretation =this.pronom+" est en situation "+ "d' obésité modéré";
    } else if ((prmIMC >= 35) && (prmIMC <= 40)) {
        interpretation =this.pronom+" est en situation "+ "d' obésité severe";
    } else if (prmIMC > 40) {
        interpretation = this.pronom+" est en situation "+ "d'Obésité morbide";
    }
    return interpretation;
        
    }




};*/

//version 2
let objPatient = {
    nom: 'Dupond',
    prenom: 'jean',
    age: 30,
    sexe: 'masculin',
    pronom: 'il',
    taille: 180, //cm
    poids: 85, //kg
    decrire: function () {
        let description = "";
        description = "le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. Il mesure " + this.taille + " cm et pèse " + this.poids + " kg";
        return description;
    },
    definir_corpulence: function () {


          function CalculerImc() {
            let calculimc = 0;
            calculimc = objPatient.poids / ((objPatient.taille / 100) * (objPatient.taille / 100));
            return calculimc;
        }
        let imc = CalculerImc().toFixed(2);
        function interpreter_IMC (prmIMC) {
            let interpretation = "";
        if (prmIMC < 16.5) {
            interpretation =objPatient.pronom+" est en situation "+ "de denutrition";
        } else if ((prmIMC >= 16.5) && (prmIMC < 18.5)) {
            interpretation =objPatient.pronom+" est en situation "+ "de maigreur";
        } else if ((prmIMC >= 18.5) && (prmIMC < 25)) {
            interpretation =objPatient.pronom+" est en situation "+ "de corpulence normal";
        } else if ((prmIMC >= 25) && (prmIMC < 30)) {
            interpretation =objPatient.pronom+" est en situation "+ "de surpoids";
        } else if ((prmIMC >= 30) && (prmIMC < 35)) {
            interpretation =objPatient.pronom+" est en situation "+ "d' obésité modéré";
        } else if ((prmIMC >= 35) && (prmIMC <= 40)) {
            interpretation =objPatient.pronom+" est en situation "+ "d' obésité severe";
        } else if (prmIMC > 40) {
            interpretation = objPatient.pronom+" est en situation "+ "d'Obésité morbide";
        }
        return interpretation;
            
        }
        let interpret = interpreter_IMC(imc);
        let affichage ="Son IMC est de: "+imc+" \n"+interpret;
        return affichage;
    }



};

console.log(objPatient.decrire());
console.log(objPatient.definir_corpulence());
