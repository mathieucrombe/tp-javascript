/*
Auteur : Mathieu Crombe
Date : 06/04/2021
*/
let taille = 160; //en cm
let poids = 100; // en kg
let IMC = poids/((taille /100)*(taille/100));
console.log("Calcul de l'imc :");
console.log("Taille : " +taille +"cm");
console.log("Poids : " +poids +"kg");
console.log("IMC = " +IMC.toFixed(1));