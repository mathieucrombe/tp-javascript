/*
Auteur : Mathieu Crombe
Date : 06/04/2021
*/
let dollars = 0;

for (let i = 1; i <  16385 ; i=i*2) {
    dollars = i * 1.65;
    console.log(i +" euro(s) = " +dollars.toFixed(2) +" dollar(s)");
}