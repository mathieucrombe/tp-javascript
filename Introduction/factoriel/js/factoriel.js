/*
Auteur : Mathieu Crombe
Date : 06/04/2021
*/
let chifre = 10;
let factorielle = chifre;
if (chifre>1) {
    console.log("Le nombre est strictement supérieur à 1");
    for (let index = 1; index < factorielle; index++) {    
        chifre = chifre*index;
    }
    console.log(chifre);
}
else {
    console.log("Le nombre n'est pas strictement supérieur à 1");
}