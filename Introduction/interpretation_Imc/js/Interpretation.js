/*
Auteur : Mathieu Crombe
Date : 06/04/2021
*/
let taille = 175; //en cm
let poids = 100; // en kg
let IMC = poids/((taille /100)*(taille/100));
let Message;
console.log("Calcul de l'imc :");
console.log("Taille : " +taille +"cm");
console.log("Poids : " +poids +"kg");
console.log("IMC = " +IMC.toFixed(1));

if(IMC < 16.5){
    Message = "Denutrition";
} else if((IMC >= 16.5)&&(IMC < 18.5)){
    Message = "Maigreur"
} else if((IMC >= 18.5)&&(IMC < 25)){
    Message = "corpulence normal"
} else if((IMC >= 25)&&(IMC < 30)){
    Message = "Surpoids"
} else if((IMC >= 30)&&(IMC < 35)){
    Message = "obésité modéré"
} else if((IMC >= 35)&&(IMC <= 40)){
    Message = "obésité severe"
} else if(IMC > 40){
    Message = "Obésité morbide"
}
console.log("Interprétation de l'IMC : " +Message)