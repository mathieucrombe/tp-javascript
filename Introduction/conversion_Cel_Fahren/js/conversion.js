/*
Auteur : Mathieu Crombe
Date : 06/04/2021
*/
let C = 22.6 ;
let F = C * 1.8 + 32 ;
console.log("conversion Celcius (°C) / Fahrenheit (°F)")
console.log("une température de " +C+"°C correspond à une température de " +F.toFixed(1) +"°F")