let trame_gps = "$ GPRMC, 161229.487, A, 3723.2475, N, 12158.3416, W, 0.13,309.62,120598 ,, *10\r\n";
class Decodage_tram_gps {
    constructor(prmtram) {
        this.Trame = prmtram;
        this.tabchamps = prmtram.split(",");
        this.length = this.tabchamps.length;
        this.est_rmc = false;
        this.latNS = "";
        this.longEW = "";
        this.nbMinLat = 0;
        this.nbMinLong = 0;
        this.nbDegreLat = 0;
        this.nbDegreLong = 0;
        this.jours = 0;
        this.mois = 0;
        this.anne = 0;
        this.seconde = 0.0;
        this.minutes = 0;
        this.heure = 0;
    }
    decoder() {
        let long = this.length;
        let indices = this.Trame;
        let verification = indices.includes('$ GPRMC');
        let resultat = false
        let tableaux = this.tabchamps;
        let latNS = "";
        let longEW = "";
        let nbMinLat = 0;
        let nbMinLong = 0;
        let nbDegreLat = 0;
        let nbDegreLong = 0;
        let jours = 0;
        let mois = 0;
        let annee = 0;
        let secondes = 0.0;
        let minutes = 0;
        let heures= 0;
        let notundefined = "Boom Boom";
        let tabmoi = ["Janv.", "Fevr.", "Mars", "Avr.", "Mai", "Juin", "Juil.", "Août", "Sept.", "Oct.", "Nov.", "Dec."];
        function verifierRMC() {
            if ((long == 12) && (verification == true)) {
                resultat = true;
            }
            return resultat;
        }
        this.est_rmc = verifierRMC();
        let vraioufaux = this.est_rmc;
        function extraireposition() {
            //latitude
            latNS = tableaux[4];
            nbMinLat = parseFloat(tableaux[3].substring(3, 10));
            nbDegreLat = parseInt(tableaux[3].substring(0, 3));
            //longitudde
            longEW = tableaux[6];
            nbMinLong = parseFloat(tableaux[5].substring(4, 11));
            nbDegreLong = parseInt(tableaux[5].substring(0, 4));
        }
        function extrairedate() {
            jours = parseInt(tableaux[9].substring(0, 2));
            mois = parseInt(tableaux[9].substring(2, 4));
            annee = parseInt(tableaux[9].substring(4, 6));

            mois = tabmoi[mois - 1];

            if (annee >= 50) {
                annee = 1900 + annee;
            } else {
                annee = 2000 + annee;
            }
        }
        function extraireHeure(){
            heures = parseInt(tableaux[1].substring(0,3));
            minutes = parseInt(tableaux[1].substring(3,5));
            secondes = parseFloat(tableaux[1].substring(5,11));
        }
        if (vraioufaux == true) {
            extraireposition();
            extrairedate();
            extraireHeure();
            this.latNS = latNS;
            this.nbMinLat = nbMinLat;
            this.nbDegreLat = nbDegreLat;
            this.longEW = longEW;
            this.nbMinLong = nbMinLong;
            this.nbDegreLong = nbDegreLong;
            this.jours = jours;
            this.mois = mois;
            this.anne = annee;
            this.seconde = secondes;
            this.minutes = minutes;
            this.heure = heures;

        }
        return notundefined;
    }
    lirelatitude() {
        let affichage = "";
        affichage = "Latitude : " + this.nbDegreLat + "° " + this.nbMinLat + "'" + this.latNS;
        return affichage
    }
    lirelongitude() {
        let affichage = "";
        affichage = "Longitude : " + this.nbDegreLong + "° " + this.nbMinLong + "'" + this.longEW;
        return affichage;
    }
    lireDate(){
        let affichage;
        affichage = this.jours+" "+this.mois+" "+this.anne;
        return affichage;
    }
    lireheure(){
        let affichage;
        affichage= this.heure+" H "+this.minutes+" mn "+this.seconde+" s";
        return affichage;
    }
}

let objdecodage_trame_gps = new Decodage_tram_gps(trame_gps);
console.log(objdecodage_trame_gps);
console.log(objdecodage_trame_gps.decoder());
console.log("Information contenue dans la tram :\n  Position: \n        " + objdecodage_trame_gps.lirelatitude() + "\n        " + objdecodage_trame_gps.lirelongitude()+"\n  Date: \n        "+objdecodage_trame_gps.lireDate()+"\n  Heure: \n        "+objdecodage_trame_gps.lireheure());
