/*
Auteur : Mathieu Crombe
Date : 08/04/2021
*/
function CalculerImc(prmTaille, prmPoids) {
    let valIMC = 0;
    valIMC = prmPoids / ((prmTaille / 100) * (prmTaille / 100));
    return valIMC;

}

function interpreterImc(prmIMC) {
    let interpretation = "";
    if (prmIMC < 16.5) {
        interpretation = "Denutrition";
    } else if ((prmIMC >= 16.5) && (IMC < 18.5)) {
        interpretation = "Maigreur";
    } else if ((prmIMC >= 18.5) && (IMC < 25)) {
        interpretation = "corpulence normal";
    } else if ((prmIMC >= 25) && (IMC < 30)) {
        interpretation = "Surpoids";
    } else if ((prmIMC >= 30) && (IMC < 35)) {
        interpretation = "obésité modéré";
    } else if ((prmIMC >= 35) && (IMC <= 40)) {
        interpretation = "obésité severe";
    } else if (prmIMC > 40) {
        interpretation = "Obésité morbide";
    }
    return interpretation;
}


let taille = 175; //en cm
let poids = 100; // en kg
let IMC = CalculerImc(taille, poids);
let Message = interpreterImc(IMC);




console.log("Calcul de l'imc :");
console.log("Taille : " + taille + "cm");
console.log("Poids : " + poids + "kg");
console.log("IMC = " + IMC.toFixed(1));

console.log("Interprétation de l'IMC : " + Message)