/*
Auteur : Mathieu Crombe
Date : 08/04/2021
*/
let cherche = 3;
let limite = 20;                                    //valeurs limite
let Multiple = "";                                  //valeurs d'affichage
function mult(prmlimite,prmmultiple) {                          //fontctions
    let affichage = "";                             //valeurs affichage
    for (let i = 0; i < prmlimite; i++) {           //boucle
        if (i == 0) {                               //pour eviter les traits devant le 0
            affichage = affichage + i;              //l'affichage san sans le traits
        } else if (i % prmmultiple == 0) {                    //si pour les multiples
            affichage = affichage+ "-" + i ;        //affichage
        }
    }
    return affichage;                               //retour
}

Multiple = mult(20,3);                              //utilisation fonctions
console.log("Recherche des multiples de "+cherche+" :");   
console.log("Valeurs limite de la recherche : "+limite);  
console.log("Multiple de "+cherche+" : "+Multiple);                              
