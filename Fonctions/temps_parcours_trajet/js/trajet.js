/*
Auteur : Mathieu Crombe
Date : 08/04/2021
*/
let vitesse = 90;   //en km/h
let distance = 500;  //en km



function calculerTempsParcoursSec(prmVitesse,prmDistance) {                    
    let kms = prmVitesse /3600;     //vitesse en km/s
    let ts = prmDistance / kms;     //le temps total en seconde

}
function convertir_h_min_s(prmV, prmD) {
    affichage = "";
    let kms = prmV /3600;                                                   //vitesse en km/s
    let ts = prmD/ kms;                                                     //le temps total en seconde
    let tm = ts /60 ;                                                       //le temps total en minute
    let s = ts % 60 ;                                                       //les secondes présente 
    let m =tm % 60 ;                                                        //les minutes présentes
    let h = tm / 60 ;                                                       //les heures présentes
  
    affichage = Math.floor(h)+"h "+Math.floor(m)+"m "+Math.floor(s)+"s ";   //affichage en heures minute secondes arondie à l'inferieure
    return affichage;

}
let temps = calculerTempsParcoursSec(vitesse,distance);    
let hms = convertir_h_min_s(vitesse, distance);
console.log("calcul du temps de parcours d'un trajet :");
console.log("   Vitesse moyenne (en km/h) :         "+vitesse);
console.log("   Distance à parcourir (en km) :      "+distance);
console.log("A "+vitesse+" km/h, une distance de "+distance+" km est parcourue en "+hms);
