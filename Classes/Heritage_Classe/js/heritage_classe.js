class personne {
    constructor(prmNom,prmPrenom,prmAge,prmSexe) {
        this.nom = prmNom;
        this.prenom = prmPrenom;
        this.age = prmAge;
        this.sexe = prmSexe;
    }
    decrire(){
        let description;
        description = "Cette personne s'appelle " + this.prenom + " " + this.nom + " elle est agée de " + this.age + " ans" ;
        return description;
    }
}

class professeur extends personne{
    constructor(prmNom, prmPrenom, prmAge, prmSexe, prmMatiere){
        super(prmNom,prmPrenom,prmAge,prmSexe);
        this.Matiere = prmMatiere;
    }
    decrire_plus(){
        let description;
    let prefixe;
    if((this.sexe == 'masculin')||(this.sexe == 'Masculin')) {
        prefixe = 'Mr';
    } else if((this.sexe == 'feminin')||(this.sexe == 'Feminin')){
        prefixe = 'Mme';
    }
    description = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.Matiere;
    return description;
    }
}

class eleve extends personne{
    constructor(prmNom, prmPrenom, prmAge, prmSexe,prmclasse){
        super(prmNom,prmPrenom,prmAge,prmSexe);
        this.classe = prmclasse;
    }
    decrire_plus(){
        let description;
    let prefixe;
    if((this.sexe == 'masculin')||(this.sexe == 'Masculin')) {
        prefixe = 'Un eleve';
    } else if((this.sexe == 'feminin')||(this.sexe == 'Feminin')){
        prefixe = 'Une eleve';
    }
    description =this.prenom + " " + this.nom + " est "+prefixe +" de "+ this.classe;
    return description;
    }
}

let objProf = new professeur('issou','Proffesseur',28,'Masculin','Informatique');
let objeleve = new eleve('Crombé','Mathieu',19,'Masculin','SNIR1');
console.log(objProf.decrire());
console.log(objProf.decrire_plus());
console.log(objeleve.decrire());
console.log(objeleve.decrire_plus());