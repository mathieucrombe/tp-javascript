class personnage{
    constructor(prmNom,PrmNiveau){
        this.Nom = prmNom;
        this.Niveau = PrmNiveau;
    }
    saluer(){
        let saluer;
        saluer = this.Nom+" Héros de Niveau "+this.Niveau+" vous salue!!"
        return saluer;
    }
}

class guerrier extends personnage{
    constructor(prmNom,prmNiveau,prmArmes){
        super(prmNom,prmNiveau);
        this.Armes = prmArmes;
    }
    description(){
        let dialogue="";
        dialogue = this.Nom + ' est un guerrier qui se bat avec '+this.Armes;
        return dialogue;
    }
}

class mage extends personnage{
    constructor(prmNom,prmNiveau,prmSpell){
        super(prmNom,prmNiveau);
        this.Spell = prmSpell;
    }
    description(){
        let dialogue="";
        dialogue = this.Nom + ' est un magicien qui posséde le pouvoir de '+this.Spell;
        return dialogue;
    }
}

let objGuerrier1 = new guerrier('Leroy Jenkins', 100,'Tranches Dragons')
let objMaje1 = new mage('Ecathe','∞','Illusion');
console.log(objGuerrier1.saluer());
console.log(objGuerrier1.description());
console.log(objMaje1.saluer());
console.log(objMaje1.description());
