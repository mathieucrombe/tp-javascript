class patient {
    constructor(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids, prmhop) {
        this.nom = prmNom;
        this.prenom = prmPrenom;
        this.age = prmAge;
        this.lieux = prmhop;
        this.sexe = prmSexe;
        this.taille = prmTaille; //cm
        this.poids = prmPoids; //kg
        this.IMC = this.poids / ((this.taille / 100) * (this.taille / 100));
    }
    decrire() {
        let genre = this.sexe;
        let description;
        if ((genre == 'Masculin') || (genre == 'masculin')) {
            description = "le patient " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agé de " + this.age + " ans. il mesure " + this.taille + " cm et pèse " + this.poids + " kg.\n"+"Il est actuellement interné à "+this.lieux+".\n"+"Son IMC est de : "+this.IMC.toFixed(2);
        } else if ((genre == 'Feminin') || (genre == 'feminin')) {
            description = "la patiente " + this.prenom + " " + this.nom + " de sexe " + this.sexe + " est agée de " + this.age + " ans. elle mesure " + this.taille + " cm et pèse " + this.poids + " kg.\n"+"Ellle est actuellement interné à "+this.lieux+".\n"+"Son IMC est de : "+this.IMC.toFixed(2);
        }
        return description;
    }
    corpulence(){
        let gendre=this.sexe;
        let interpretation = "";
        if ((gendre == 'Masculin') || (gendre == 'masculin')) {
            if (this.IMC < 16.5) {
                interpretation = "Il est en situation " + "de denutrition";
            } else if ((this.IMC >= 16.5) && (this.IMC < 18.5)) {
                interpretation = "Il est en situation " + "de maigreur";
            } else if ((this.IMC >= 18.5) && (this.IMC < 25)) {
                interpretation = "Il est en situation " + "de corpulence normal";
            } else if ((this.IMC >= 25) && (this.IMC < 30)) {
                interpretation = "Il est en situation " + "de surpoids";
            } else if ((this.IMC >= 30) && (this.IMC < 35)) {
                interpretation = "Il est en situation " + "d' obésité modéré";
            } else if ((this.IMC >= 35) && (this.IMC <= 40)) {
                interpretation = "Il est en situation " + "d' obésité severe";
            } else if (this.IMC > 40) {
                interpretation = "Il est en situation " + "d'Obésité morbide";
            }
        } else if ((gendre == 'Feminin') || (gendre == 'feminin')) {
            if (this.IMC < 14.5) {
                interpretation = "Elle est en situation " + "de denutrition";
            } else if ((this.IMC >= 14.5) && (this.IMC < 16.5)) {
                interpretation = "Elle est en situation " + "de maigreur";
            } else if ((this.IMC >= 16.5) && (this.IMC < 23)) {
                interpretation = "Elle est en situation " + "de corpulence normal";
            } else if ((this.IMC >= 23) && (this.IMC < 28)) {
                interpretation = "Elle est en situation " + "de surpoids";
            } else if ((this.IMC >= 28) && (this.IMC < 33)) {
                interpretation = "Elle est en situation " + "d' obésité modéré";
            } else if ((this.IMC >= 33) && (this.IMC <= 38)) {
                interpretation = "Elle est en situation " + "d' obésité severe";
            } else if (this.IMC > 38) {
                interpretation = "Elle est en situation " + "d'Obésité morbide";
            }
        }
        return interpretation;
    }
}

let objInterne1 = new patient('Crombe','Mathieu',19,'Masculin',187,66,'Gustave Eiffel');

console.log(objInterne1.decrire());
console.log(objInterne1.corpulence());