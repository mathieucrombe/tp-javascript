# TP-JavaScript

Mon TP JavaScript

> * Auteur : Mathieu Crombe
> * Date de publication : 06/04/2021

## Sommaire

- Introduction
  - [HelloWorld](Introduction/HelloWorld
  )
  - [Calcul de surface](Introduction/calcul_surface)
  - [calcul d'IMC](Introduction/calculIMC)
  - [Conversion Celcius to Fahrenheit](Introduction/conversion_Cel_Fahren)
  - [Interpretation IMC](Introduction/interpretation_Imc)
  - [Suite syracus](Introduction/Syracuse)
  - [Calcul de factorielle](Introduction/factoriel)
  - [Conversion Euros Dollars](Introduction/ConversionED)
  - [Nombres triples](Introduction/NombresTriples)
  - [Suite de Fibonacci](Introduction/Suite%20de%20Fibonaci)
  - [Table de Multiplication](Introduction/TableMulti)
- Fonctions
  - [Fonctions interpretations IMC](Fonctions/imc)
  - [fonctions trajets](Fonctions/temps_parcours_trajet)
  - [Multiple de 3](Fonctions/multiplede3)
- Objets
  - [Calcul d'IMC](obj/IMC) 
  - [Calcul d'IMC v2](obj/IMCv2)
  - [Calcul d'IMC v3](obj/IMCv3)
  - [Héritage et Prototype](obj/Heritage_Proto)
  - [Création de perssonage](obj/jeux)
- Tableaux et Array
  - [Calcul de moyenne](tableaux/calcul_moyenne)
  - [Liste d'Articles](tableaux/Liste_Articles)
  - [Liste des patients](tableaux/Tableau_Patient_IMC)
  - [Histogramme](tableaux/Histogramme)
- Les Classes
  - [Calcul d'IMC](Classes/classeIMC)
  - [Héritage et Classe](Classes/Heritage_Classe)
  - [RPG 2.164535](Classes/RPG)
- Les chaines de charactères
  - [Les chaines de charactères](chainne_chara/decodage_trame_gps)